package algorithms;

import java.awt.Point;
import java.util.ArrayList;


/*
 * - /!\ Pour la version budget du calculSteiner, il y a une piste que j'ai explorer mais n'ai pas eu le temps d'implémenter.
 * Afin d'obtenir le meilleur résultat lors de la conversion des edges en arbre. Nous pourrions utiliser une structure de liste d'adjacence.
 * Nous pourrions donc facilement reconstituer la liste des edges appartenant à une liste de point.
 * 
 * Soit Lp une liste de point
 * Soit Le une liste d'edge trié dans l'ordre croissant en fonction de leur poids
 * Soit T un arbre
 * 
 * Tant que Le n'est pas vide:
 * 		Pour i allant de 0 à len(Le) :
 * 			Si Le[i].p ou Le[i].q appartient à Lp:
 * 				Si BudgetEnCours + Le[i].poids < BudgetTotal
 * 					Lp.add(Le[i].q ou Le[i].p)
 * 					BudgetEnCours += Le[i].poids
 * 					AddInTree(T,Le[i])					
 * 					Le.remove[i]
 * 					continue
 * 				Sinon :
 * 					break
 * 
 *	De cette sorte, nous ajoutons à chaque fois la plus petite edge respectant le budget et appartenant à l'arbre en cours
 */

public class DefaultTeam {
	
	private static int B = 1664;
	
	public Tree2D calculSteiner(ArrayList<Point> points, int edgeThreshold, ArrayList<Point> hitPoints) {

		// On génère un plus court chemin all-to-all sous la même forme que le TME10
		int[][] FloydWarshall = calculShortestPaths(points, edgeThreshold);
		
		
		// On construit le sous-graphe complet composé des points de hitPoints
		ArrayList<Edge> edges = new ArrayList<Edge>();
		for (Point p : hitPoints) {
			for (Point q : hitPoints) {
				if (p.equals(q) || contains(edges, p, q))
					continue;
				edges.add(new Edge(p, q));
			}
		}
		
		
		// Il s'agit là de construire le chemin d'un hitPoint vers l'autre
		// Voir dans la classe Edge pour plus d'informations
		for (Edge e : edges) {
			e.setPath(points, FloydWarshall);
		}
		
		// On trie les edges dans l'ordre croissant pour appliquer un Kruskal
		edges = sort(edges);
		

		// Construction de kruskal basé sur le TME09 avec les edges de hitPoints en hitPoints
		ArrayList<Edge> kruskal = new ArrayList<Edge>();
		Edge current;
		NameTag forest = new NameTag(hitPoints);

		while (edges.size() != 0) {
			current = edges.remove(0);
			if (forest.tag(current.p) != forest.tag(current.q)) {
				kruskal.add(current);
				forest.reTag(forest.tag(current.p), forest.tag(current.q));
			}
		}
		
		Tree2D firstTree = edgesToTreeWithFloyd(kruskal, kruskal.get(0).p);	
		return firstTree;
	}
	
	private Tree2D edgesToTreeWithFloyd(ArrayList<Edge> edges, Point root) {
		ArrayList<Edge> fullEdges = new ArrayList<Edge>();
		
		/*
		 * A travers cette fonction, nous allons reconstituer les chemins entre les edges dans le graphes. 
		 * Jusqu'alors les edges étaient considérer comme la distance à vol d'oiseau d'un hitPoint à l'autre.
		 * Ici nous allons donc remplacer chaque Edge de P vers Q par la suite d'Edge formant le plus court chemin de P vers Q
		 * Sans ajouter de doublons pour éviter de traiter deux fois les mêmes edges
		 * */
		
		for(Edge e : edges) {
			for (Edge ep : e.path) {
				if(!fullEdges.contains(ep)){
					fullEdges.add(ep);
				}
			}
		}
		
		// Une fois les chemins les plus courts ajoutés, nous allons procéder à la reconstruction de l'arbre.
		return edgesToTree(fullEdges,root);	
	}
	
	@SuppressWarnings("unchecked")
	private Tree2D edgesToTree(ArrayList<Edge> edges, Point root) {
		ArrayList<Edge> remainder = new ArrayList<Edge>();
		ArrayList<Point> subTreeRoots = new ArrayList<Point>();
		Edge current;
		
		/*
		 * Comme dans le TME9 sur Kruskal, nous allons reconstituer l'arbre avec les edges constituants les plus courts chemins
		 * subTreeRoots servira pour stocker les points qui sont les sucesseurs de la racine
		 * remainder stockera les arrêtes qui n'ont pas encore été traitées
		 */

		while (edges.size() != 0) {
			current = edges.remove(0);
			if (current.p.equals(root)) {
				subTreeRoots.add(current.q);
			} else {
				if (current.q.equals(root)) {
					subTreeRoots.add(current.p);
				} else {
					remainder.add(current);
				}
			}
		}
		
		// Ensuite nous traitons également les successeurs de la racine par récursivité
		ArrayList<Tree2D> subTrees = new ArrayList<Tree2D>();
		
		for (Point subTreeRoot : subTreeRoots)
			subTrees.add(edgesToTree((ArrayList<Edge>) remainder.clone(), subTreeRoot));
		return new Tree2D(root, subTrees);
	}
	
	
	// Si vous souhaitez modifier le budget. Modifiez la variable statique B qui se trouve tout en haut de la classe.
	public Tree2D calculSteinerBudget(ArrayList<Point> points, int edgeThreshold, ArrayList<Point> hitPoints) {
		
		// Même principe que pour le calculSteiner
		int[][] FloydWarshall = calculShortestPaths(points, edgeThreshold);
		
		ArrayList<Edge> edges = new ArrayList<Edge>();
		for (Point p : hitPoints) {
			for (Point q : hitPoints) {
				if (p.equals(q) || contains(edges, p, q))
					continue;
				edges.add(new Edge(p, q));
			}
		}
		
				
		for (Edge e : edges) {
			e.setPath(points, FloydWarshall);
		}
		edges = sort(edges);
		

		ArrayList<Edge> kruskal = new ArrayList<Edge>();
		Edge current;
		NameTag forest = new NameTag(hitPoints);
			
		
		while (edges.size() != 0) {
			current = edges.remove(0);
			if (forest.tag(current.p) != forest.tag(current.q)) {
				kruskal.add(current);
				forest.reTag(forest.tag(current.p), forest.tag(current.q));
			}
		}
		
		// On commence avec comme racine, le premier point de hitPoint comme indiqué dans l'énoncé.
		Point start = hitPoints.get(0);
		
		// On va appliquer un filtre, qui va nous faire ressortir toutes les Edges de l'arbre avec le budget B = (par défault : 1664)
		ArrayList<Edge> budgetEdges = edgesToEdgesWithFloydBudget(kruskal, start, DefaultTeam.B);	
		
		
		budgetEdges = sort(budgetEdges);
		// Puis on construit l'arbre de la même manière que pour le calcul Steiner.
		Tree2D firstTree = edgesToTreeWithFloyd(budgetEdges,start);
		
		return firstTree;
	  }
	
	
	private boolean contains(ArrayList<Edge> edges, Point p, Point q) {
		for (Edge e : edges) {
			if (e.p.equals(p) && e.q.equals(q) || e.p.equals(q) && e.q.equals(p))
				return true;
		}
		return false;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	private ArrayList<Edge> edgesToEdgesWithFloydBudget(ArrayList<Edge> edges, Point root, double maxBudget) {
		ArrayList<Edge> remainder = new ArrayList<Edge>();
		ArrayList<Point> subTreeRoots = new ArrayList<Point>();
		ArrayList<Edge> edgeOfTree = new ArrayList<Edge>();
		Edge current;
		
		double budget = 0;
		
		/*
		 * Comme dans le TME9 sur Kruskal, nous allons reconstituer l'arbre avec les edges constituants les plus courts chemins
		 * subTreeRoots servira pour stocker les points qui sont les sucesseurs de la racine
		 * remainder stockera les arrêtes qui n'ont pas encore été traitées
		 * 
		 * 
		 * edgeOfTree permettra d'ajouter au fur et a mesure les arrêtes correspondants aux points sucesseurs
		 */

		while (edges.size() != 0) {
			current = edges.remove(0);
							
			if (current.p.equals(root)) {
				
				// On verifie si le budget est respecté
				if(budget + current.distance < maxBudget) {
					
					// Ajout du point + edge
					subTreeRoots.add(current.q);
					if(!edgeOfTree.contains(current)) {
						edgeOfTree.add(current);
					}
					budget = budget + current.distance;
				}
				
			} else {
				if (current.q.equals(root)) {
					
					// On verifie si le budget est respecté
					if(budget + current.distance < maxBudget) {
						
						// Ajout du point + edge
						subTreeRoots.add(current.p);
						if(!edgeOfTree.contains(current)) {
							edgeOfTree.add(current);
						}
						budget = budget + current.distanceEucl();
					}
				} else {
					remainder.add(current);
				}
			
			}
		}
		
		/*
		 * Il s'agit ici de faire le même traîtement d'edge pour les sucesseurs
		 */
		ArrayList<Edge> moreEdges = new ArrayList<Edge>();
		for (Point subTreeRoot : subTreeRoots) {
			ArrayList<Edge> subEdges= edgesToEdgesWithFloydBudget((ArrayList<Edge>) remainder.clone(), subTreeRoot, maxBudget - budget);
			for(Edge e : subEdges) {
				moreEdges.add(e);
			}
		}
		
		
		
		/*
		 * On ajoute ensuite ces edges successeurs à la liste d'edges filtrés
		 */
		
		for(Edge e : moreEdges) {
			if(budget + e.distance < maxBudget) {
				edgeOfTree.add(e);
				budget = budget + e.distance;
			}else {
				break;
			}
		}
		
		return edgeOfTree;
	}
	
	private ArrayList<Edge> sort(ArrayList<Edge> edges) {
		if (edges.size() == 1)
			return edges;

		ArrayList<Edge> left = new ArrayList<Edge>();
		ArrayList<Edge> right = new ArrayList<Edge>();
		int n = edges.size();
		for (int i = 0; i < n / 2; i++) {
			left.add(edges.remove(0));
		}
		while (edges.size() != 0) {
			right.add(edges.remove(0));
		}
		left = sort(left);
		right = sort(right);

		ArrayList<Edge> result = new ArrayList<Edge>();
		while (left.size() != 0 || right.size() != 0) {
			if (left.size() == 0) {
				result.add(right.remove(0));
				continue;
			}
			if (right.size() == 0) {
				result.add(left.remove(0));
				continue;
			}
			if (left.get(0).distance < right.get(0).distance)
				result.add(left.remove(0));
			else
				result.add(right.remove(0));
		}
		return result;
	}

	// all-to-all basé sur le TME10
	public int[][] calculShortestPaths(ArrayList<Point> points, int edgeThreshold) {
		
		int[][] paths = new int[points.size()][points.size()];
		for (int i = 0; i < paths.length; i++)
			for (int j = 0; j < paths.length; j++)
				paths[i][j] = i;

		double[][] dist = new double[points.size()][points.size()];

		for (int i = 0; i < paths.length; i++) {
			for (int j = 0; j < paths.length; j++) {
				if (i == j) {
					dist[i][i] = 0;
					continue;
				}
				if (points.get(i).distance(points.get(j)) <= edgeThreshold)
					dist[i][j] = points.get(i).distance(points.get(j));
				else
					dist[i][j] = Double.POSITIVE_INFINITY;
				paths[i][j] = j;
			}
		}

		for (int k = 0; k < paths.length; k++) {
			for (int i = 0; i < paths.length; i++) {
				for (int j = 0; j < paths.length; j++) {
					if (dist[i][j] > dist[i][k] + dist[k][j]) {
						dist[i][j] = dist[i][k] + dist[k][j];
						paths[i][j] = paths[i][k];

					}
				}
			}
		}

		return paths;
	}

}

class Edge implements java.lang.Comparable<Edge>{
	protected Point p, q;
	
	// path nous servira pour construire les edges intermédiaires permettant de mener du point P au point Q à travers un graphe donné
	protected ArrayList<Edge> path;
	protected double distance;

	protected Edge(Point p, Point q) {
		this.p = p;
		this.q = q;
		
	}
	
	
	// Rend la distance euclidienne entre le point P et le point Q
	public double distanceEucl() {
		return this.p.distance(this.q);
	}
	
	
	public boolean equals(Object obj) {
		if (obj instanceof Edge) {
			if( ((Edge)obj).p.equals(this.p) && ((Edge)obj).q.equals(this.q)) {
				return true;
			}
		}
		return false;
	}
	
	protected void setPath(ArrayList<Point> points, int[][] paths) {
		this.path = new ArrayList<Edge>();
		int indexP = points.indexOf(this.p);
		int indexQ = points.indexOf(this.q);
		
		int indexIntermOld = indexP;
		int indexInterm = paths[indexP][indexQ];
		double dist = 0;
		
		// Construction de toutes les edges intermédiaires.
		// On en profite également pour mesurer la distance au sein du graphe entre les deux points.
		do {
			dist += points.get(indexIntermOld).distance(points.get(indexInterm));
			this.path.add(new Edge(points.get(indexIntermOld), points.get(indexInterm)));
			indexIntermOld = indexInterm;
			indexInterm = paths[indexInterm][indexQ];
		}while (indexInterm != indexQ);
		
		dist += points.get(indexIntermOld).distance(points.get(indexInterm));
		
		this.distance = dist;
		this.path.add(new Edge(points.get(indexIntermOld), points.get(indexInterm)));
	}

	
	// Outil de comparaison d'edges basé sur leur poids (Distance d'un point à l'autre au sein du graphe)
	// A ne pas utiliser sans avoir setPath au préalable. Sans quoi toutes les distances seront nulles.
	@Override
	public int compareTo(Edge arg0) {
		if(this.distance < arg0.distance) {
			return -1;
		} 
		else {
			if(this.distance == arg0.distance) {
				return 0;
			}
			else {
				return 1;
			}
		}
	}
}

class NameTag {
	private ArrayList<Point> points;
	private int[] tag;

	@SuppressWarnings("unchecked")
	protected NameTag(ArrayList<Point> points) {
		this.points = (ArrayList<Point>) points.clone();
		tag = new int[points.size()];
		for (int i = 0; i < points.size(); i++)
			tag[i] = i;
	}

	protected void reTag(int j, int k) {
		for (int i = 0; i < tag.length; i++)
			if (tag[i] == j)
				tag[i] = k;
	}
	

	protected int tag(Point p) {
		for (int i = 0; i < points.size(); i++)
			if (p.equals(points.get(i)))
				return tag[i];
		return 0xBADC0DE;
	}
}
